# local changes for low effort build

# Elmyra
PRODUCT_PACKAGES += \
    ElmyraService

# add gesture typing libs
# some other gapps builds already include this
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/libjni_latinimegoogle.so:product/lib64/libjni_latinimegoogle.so

# flag for gapps
ifeq ($(WITH_GMS), true)
    $(call inherit-product-if-exists, vendor/gapps/arm64/arm64-vendor.mk)
    $(call inherit-product-if-exists, vendor/gapps/common/common-vendor.mk)
endif

$(call inherit-product-if-exists, vendor/low_effort/device-vendor.mk)
